def func3(x, y):
    if x == 0:
        return y
    else:
        return func3(x-1, x+y)


print(func3(6, 2))
