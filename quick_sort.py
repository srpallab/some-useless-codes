def partition(array, start, end):
    pivot = array[end]
    pivot_index = start
    # print(f"START: {start}, END: {end}, P: {pivot}")
    for i in range(start, end):
        # print(f"I: {i}, PI: {pivot_index}")
        if array[i] < pivot:
            array[i], array[pivot_index] = array[pivot_index], array[i]
            pivot_index += 1
    array[end], array[pivot_index] = array[pivot_index], array[end]
    # print(f"PI: {pivot_index}, A: {array}")
    return pivot_index


def quick_sort(array, start, end):
    if start < end:
        pivot_index = partition(array, start, end)
        quick_sort(array, start, pivot_index - 1)
        quick_sort(array, pivot_index + 1, end)


if __name__ == '__main__':
    # array = [-0.02, 37.2, 37.21, 33, 3, 4, 5, 44, 90, -3, 1, 0, -3, -1, 7]
    # array = [7, 5, 1, 2, 9, 0, 10, 3]
    array = [-2, 12, 45, -9, 1, 5, -2, -2]
    quick_sort(array, 0, len(array) - 1)
    array_rem = []
    for i in range(len(array)-1):
        if array[i] != array[i+1]:
            array_rem.append(array[i])
    array_rem.append(array[len(array) - 1])

    print(array)
    print(array_rem)
