def selection_sort(array, size):
    for i in range(size-1):
        # print(f"I: {i}")
        i_min = i
        for j in range(i+1, size):
            # print(f"J: {j}")
            if array[j] < array[i_min]:
                i_min = j
        array[i], array[i_min] = array[i_min], array[i]
        print(array)
    return array


if __name__ == '__main__':
    array = [-0.02, 37.2, 37.21, 290, 33, 3, 4, 5, 44, 90, 2, 1, 0, -3, -1, 7]
    sorted_array = selection_sort(array, len(array))
    print(sorted_array)
