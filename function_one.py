def func1(x):
    r = 1
    r += x
    if x > 4 and x < 10:
        r += 2 * x
    elif x <= 4:
        r += 3 * x
    else:
        r += 4 * x
    return r


print(func1(10))
