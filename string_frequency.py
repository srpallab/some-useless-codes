# L = "CLeeeEEMMMss"
L = "ccccOddEEE"
D = {}

for i in L:
    if i in D:
        D[i] += 1
    else:
        D[i] = 1

print("".join([x + str(y) for x, y in D.items()]))
