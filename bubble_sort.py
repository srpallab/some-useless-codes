def bubble_sort(array):
    for i in range(len(array)):
        # print(f"I: {i}")
        for j in range((len(array)-1)-i):
            # print(f"J: {j}")
            if array[j] > array[j+1]:
                array[j], array[j+1] = array[j+1], array[j]
            print(array)
    return array


if __name__ == '__main__':
    array = [-0.02, 37.2, 37.21, 290, 33, 3, 4, 5, 44, 90, 2, 1, 0, -3, -1, 7]
    sorted_array = bubble_sort(array)
    print(sorted_array)
