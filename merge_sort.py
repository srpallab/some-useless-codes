def merge(array, left, right):
    print(f"Left: {left}, Right: {right}")
    k = i = j = 0
    while (i < len(left) and j < len(right)):
        if left[i] > right[j]:
            array[k] = right[j]
            j += 1
        else:
            array[k] = left[i]
            i += 1
        k += 1
    while i < len(left):
        array[k] = left[i]
        i += 1
        k += 1
    while j < len(right):
        array[k] = right[j]
        j += 1
        k += 1


def merge_sort(array):
    if len(array) < 2:
        return
    mid = int(len(array)/2)
    left = array[:mid]
    right = array[mid:]
    merge_sort(right)
    merge_sort(left)
    merge(array, left, right)


if __name__ == '__main__':
    array = [-0.02, 37.2, 37.21, 290, 33, 3, 4, 5, 44, 90, 2, 1, 0, -3, -1, 7]
    # array = [7, 5, 1, 2, 9, 0, 10, 3]
    merge_sort(array)
    print(array)
