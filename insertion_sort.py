def insertion_sort(array, size):
    for i in range(1, size):
        hole = -1
        value = array[i]
        for j in range(i, 0, -1):
            if array[j-1] > value:
                hole = j-1
                array[j-1], array[j] = array[j], array[j-1]
                print(array)
        if hole > -1:
            array[hole] = value
    return array


if __name__ == '__main__':
    array = [-0.02, 37.2, 37.21, 290, 33, 3, 4, 5, 44, 90, 2, 1, 0, -3, -1, 7]
    # array = [7, 5, 1, 2, 9, 0, 10, 3]
    sorted_array = insertion_sort(array, len(array))
    print(sorted_array)
